package com.sar.cardesign;

import java.util.HashMap;
import java.util.Map;

public class Mondeo implements Car {
	private HashMap<String, Feature> features = new HashMap<>();

	public Mondeo() {
		features.put("MD1", new Feature("red",200000,"Sedan"));
		features.put("MD2", new Feature("Blue",400000,"SUV"));
		features.put("MD3", new Feature("Yellow",200000,"HatchBack"));
	}

	@Override
	public Map<String, Feature> getModelFeatures() {
		// TODO Auto-generated method stub
		return features;
	}

}
