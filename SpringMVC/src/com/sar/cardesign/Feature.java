package com.sar.cardesign;

public class Feature {
	private String color;
	private double price;
	private String category;
	
	public Feature(String color, double price, String category) {
		this.color = color;
		this.price=price;
		this.category=category;
		
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
	public String toString()
	{
		return "color : " + color + " price: " + price + " category" + category;
		
	}

}
