package com.sar.cardesign;

import java.util.Map;

public class CarFactory {

	public static Map<String, Feature> getCarFeatures(String modelName) {
		final Car carInstance = getCarInstance(modelName);
		return carInstance.getModelFeatures();
	}

	private static Car getCarInstance(String modelName) {
		Car carInstance = null;
		if (modelName.equalsIgnoreCase("Ecosport")) {
			carInstance = new Ecosport();
		} else if (modelName.equalsIgnoreCase("Endeavour")) {
			carInstance = new Endeavour();
		} else if (modelName.equalsIgnoreCase("Figo")) {
			carInstance = new Figo();
		} else if (modelName.equalsIgnoreCase("Mondeo")) {
			carInstance = new Mondeo();
		}
		return carInstance;
	}
}
