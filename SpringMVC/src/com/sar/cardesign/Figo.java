package com.sar.cardesign;

import java.util.HashMap;
import java.util.Map;

public class Figo implements Car {
	private HashMap<String, Feature> features = new HashMap<>();

	public Figo() {
		features.put("FG1", new Feature("red",200000,"Sedan"));
		features.put("FG2", new Feature("Blue",400000,"SUV"));
		features.put("FG3", new Feature("Yellow",200000,"HatchBack"));
	}

	@Override
	public Map<String, Feature> getModelFeatures() {
		// TODO Auto-generated method stub
		return features;
	}

}
