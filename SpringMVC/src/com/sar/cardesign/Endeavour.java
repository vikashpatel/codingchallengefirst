package com.sar.cardesign;

import java.util.HashMap;
import java.util.Map;

public class Endeavour implements Car {
	private HashMap<String, Feature> features = new HashMap<>();

	public Endeavour() {
		features.put("ED1", new Feature("red",200000,"Sedan"));
		features.put("ED2", new Feature("Blue",400000,"SUV"));
		features.put("ED3", new Feature("Yellow",200000,"HatchBack"));
	}

	@Override
	public Map<String, Feature> getModelFeatures() {
		// TODO Auto-generated method stub
		return features;
	}

}
