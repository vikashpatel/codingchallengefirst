package com.sar.cardesign;

import java.util.HashMap;
import java.util.Map;

public class Ecosport implements Car {

	private HashMap<String, Feature> features = new HashMap<>();
	
	public Ecosport() {
		features.put("EC1", new Feature("red",200000,"Sedan"));
		features.put("EC2", new Feature("Blue",400000,"SUV"));
		features.put("EC3", new Feature("Yellow",200000,"HatchBack"));
	}
	
	@Override
	public Map getModelFeatures(){
		return features;
	}

	
}
