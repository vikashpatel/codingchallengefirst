package com.sar.cardesign;

import java.util.Map;

public interface Car {
	// car Interface
Map<String,Feature> getModelFeatures();
}
