package com.sar.controller;

import java.util.Random;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.sar.model.City;

@RestController
@RequestMapping(value="api/")
@EnableWebMvc
public class TempService {
	
	@RequestMapping(value = "/getTemp", method = RequestMethod.GET)
	public @ResponseBody City getTemp(@RequestParam String cityCode, @RequestParam(defaultValue = "cels") String tempUnit) {
		City city = new City();
		city.setCityCode(cityCode);
		city.setTempUnit(tempUnit);
		if(tempUnit.equals("fahr"))
		{
			city.setTemp((new Random().ints(1, 32, 212).findFirst().getAsInt()));
		}
		city.setTemp((new Random().nextInt((100-0)+1)+0));
		System.out.println("TempService.getTemp() -> City code :" + city.getCityCode()+ " Temperature Unit :"+ city.getTempUnit() + " Temperature" + city.getTemp());
		return city;
	}
}
