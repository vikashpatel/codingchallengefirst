package com.sar.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.sar.cardesign.Feature;
import com.sar.exception.CustomGenericException;
import com.sar.model.City;

@Controller
@RequestMapping("/")
public class SimpleMVC {

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView indexMethod() {
		return new ModelAndView("index", "command", new City());
	}

	@RequestMapping(value = "/fetchTemperature", method = RequestMethod.POST)
	public ModelAndView fetchTemperature(@ModelAttribute("SpringWeb") City city) {
		if (city.getTempUnit()!=""&& !(city.getTempUnit().equalsIgnoreCase("cels") || city.getTempUnit().equalsIgnoreCase("fahr"))) {
			throw new CustomGenericException("Temperature Unit is not Proper");
		}
		// Hitting Rest Service getting city object
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8080/SpringMVC/api/getTemp/?cityCode=" + city.getCityCode() + "&tempUnit="
				+ city.getTempUnit();
		City returnedCity = restTemplate.getForObject(url, City.class);
		return new ModelAndView("displayTemperature", "returnedCity", returnedCity);
	}


	@RequestMapping(value = "/carModel", method = RequestMethod.GET)
	public ModelAndView carModel(@RequestParam(value="modelName") String modelName) {
		
		System.out.println("SimpleMVC.carModel() - > modelName : " + modelName);
		// Hitting Rest Service getting city object
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8080/SpringMVC/api/getCarFeatures?modelName=" + modelName;
		HashMap<String, Feature> featuresMap = restTemplate.getForObject(url, HashMap.class);
		return new ModelAndView("carfeatures", "featuresMap", featuresMap);
	}

	
}
