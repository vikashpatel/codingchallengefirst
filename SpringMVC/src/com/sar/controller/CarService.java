package com.sar.controller;

import java.util.HashMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.sar.cardesign.CarFactory;
import com.sar.cardesign.Feature;

@RestController
@RequestMapping(value="api/")
@EnableWebMvc
public class CarService {
	
	@RequestMapping(value = "/getCarFeatures", method = RequestMethod.GET)
	public @ResponseBody HashMap<String, Feature> getTemp(@RequestParam String modelName) {
		return (HashMap<String, Feature>) CarFactory.getCarFeatures(modelName);
	}

}
