package com.sar.model;

public class City {
String cityCode;
String tempUnit;
int temp;
public String getCityCode() {
	return cityCode;
}
public void setCityCode(String cityCode) {
	this.cityCode = cityCode;
}
public String getTempUnit() {
	return tempUnit;
}
public void setTempUnit(String tempUnit) {
	this.tempUnit = tempUnit;
}
public int getTemp() {
	return temp;
}
public void setTemp(int temp) {
	this.temp = temp;
}

}
