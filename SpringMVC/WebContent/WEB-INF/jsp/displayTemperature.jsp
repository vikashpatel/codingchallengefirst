<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<html>
   <head>
      <title>Spring MVC Form Handling</title>
   </head>

   <body>
      <h2>Extracted City Information</h2>
      <table>
         <tr>
            <td>cityCode</td>
            <td>${returnedCity.cityCode}</td>
         </tr>
         <tr>
            <td>tempUnit</td>
            <td>${returnedCity.tempUnit}</td>
         </tr>
          <tr>
            <td>temp</td>
            <td>${returnedCity.temp}</td>
         </tr>
      </table>  
   </body>
   
</html>