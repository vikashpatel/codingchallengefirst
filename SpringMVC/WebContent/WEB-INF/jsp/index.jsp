<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<html>
   <head>
      <title>Spring MVC Form Handling</title>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script>
function validateForm()
{
	if(document.frm.cityCode.value=="" || document.frm.cityCode.value.length!=3)
    {
      alert("CityCode should not be left blank and max length should be 3 chars");
      document.frm.cityCode.focus();
     return false;
    }
   /*  $.ajax({
    	
        type : 'GET',
        url : '/SpringMVC/api/getTemp',
        data : {
        	cityCode: document.frm.cityCode.value, 
        	tempUnit: document.frm.tempUnit.value
        },
       async : true,
       beforeSend: function(xhr) {
           xhr.setRequestHeader("Accept", "application/json");
           xhr.setRequestHeader("Content-Type", "application/json");
       },
       success: function(data){
           $("#div1").html('City Code : '+data.cityCode+' Temperature: '+data.temp + ' Unit: '+ data.tempUnit);
       },
       error: function (request, status, error) {
           alert(request.responseText);
       }
    }); */
}
</script>
   </head>

   <body>
   <table>
    <tr>
    <td>
      <h2>City Information</h2>
      <form:form name = "frm" action="/SpringMVC/fetchTemperature" method= "POST" onSubmit="javascript:return validateForm();">
         <table>
            <tr>
               <td><form:label path = "cityCode">City Code Name (Ex: del): </form:label></td>
               <td><form:input type="text" path="cityCode"  /></td>
            </tr>
            <tr>
               <td><form:label path = "tempUnit">Temperature Unit (Ex: cals, fahr): </form:label></td>
               <td><form:input type="text" path="tempUnit" /></td>
            </tr>
            <tr>
               <td colspan = "2">
                <!--   <input type = "Button" value= "Submit" onClick="javascript:return validateForm();"/> -->
                <input type = "submit" value= "Submit"/>
               </td>
            </tr>
         </table>  
      </form:form>
      </td>
     <!--  <div id="div1"><h2>Click Submit Button and this text will be updated by service response</h2></div> -->
     <td>
      <h2>Car Information based on Model Name</h2>
    <form name = "frm2" action="/SpringMVC/carModel" method= "GET" >
         <table>
            <tr>
               <td><label path = "carModel">Enter car model name: </label></td>
               <td><input type="text" name="modelName" /></td>
            </tr>
            
            <tr>
               <td colspan = "2">
                <!--   <input type = "Button" value= "Submit" onClick="javascript:return validateForm();"/> -->
                <input type = "submit" value= "Submit"/>
               </td>
            </tr>
         </table>  
      </form>
      </td>
   </tr>
   </table>      
   </body>
</html>