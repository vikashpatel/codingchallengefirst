<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
   <head>
      <title>Spring MVC Form Handling</title>
   </head>

   <body>
     <c:forEach var="feature" items="${featuresMap}">
    Model : ${feature.key}  - Features : ${feature.value}
    <br/>
</c:forEach>
   </body>
   
</html>